var globalVars = require("../js/globalVars.js");
var dapp = require("../js/dapp.js");
let $ = require('jquery')
let fs = require('fs')

var modal = document.getElementById('myModal');

function ownerList(etherAddress, Customer) {
  if (etherAddress) {
    let updateOptions = '<option>' + etherAddress + ', ' + Customer + '</option>';
    $('#besitzerEther').append(updateOptions);
  }
}

$('#add-new-order').on('click', () => {
  console.log('Time ' + $('#time').val())
  console.log('Date ' + $('#datetime').val())

  let datestring = $('#datetime').val().split('-')
  let datestringTime = $('#time').val().split(':');

  let epochTime = Date.UTC(datestring[0], datestring[1] - 1, datestring[2], datestringTime[0] - 1, datestringTime[1]);
  epochtime = (epochTime / 1000);
  console.log(epochtime);

  let carIdCode = $('#carIdCode').val();
  let compound = $('#compoundName').val();
  let delivery = $('#delivery input:radio:checked').val() === 'true';
  console.log(delivery)
  if(localStorage.getItem('waitingCheck') !== 'false'){
      modal.style.display = "block";
  }
  dapp.createNewOrder(carIdCode,compound,epochtime,delivery, function(res){
  })
});

$('#getAllOrder').on('click', () => {
  $("#orders-table tr>td").remove();
  dapp.getAllOrders(function (res) {
    if(localStorage.getItem('waitingCheck') !== 'false'){
        modal.style.display = "block";
    }
    console.log(res);
    let updateString = '<tr><td>' +
      res[0] + '</td><td>' +
      convertEpochToSpecificTimezone(res[1]) + '</td><td>' +
      res[2] + '</td><td>' +
      res[3] + '</td><td>' +
      res[4] + '</td></tr>';
    $('#orders-table').append(updateString);
    modal.style.display = "none";
  })
})

function loadCarIdsAndCompoundNames() {
  dapp.getAllCars(function(res) {
    console.log(res[2])
    let updateOptions = '<option>' + res[2] + '</option>';
    $('#carIdCode').append(updateOptions);
  })


  dapp.getAllCompounds(function(res) {
    console.log(res);
    let updateOptions = '<option>' + res[1] + '</option>';
    $('#compoundName').append(updateOptions);
  })
}

function convertEpochToSpecificTimezone(offset){
    var d = new Date(offset*1000);
    var utc = d.getTime() + (d.getTimezoneOffset() * 60000);  //This converts to UTC 00:00
    var nd = new Date(utc + (3600000*1));
    return nd.toLocaleString();
}


loadCarIdsAndCompoundNames();
