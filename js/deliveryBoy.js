var globalVars = require("../js/globalVars.js");
var dapp = require("../js/dapp.js");
let fs = require('fs')
let $ = require('jquery')

var modal = document.getElementById('myModal');

$('#exec-delivery').on('click', () => {
  let carIdCode = $('#carIdCode').val();
  //check if account is unlock
  dapp.isAccountLocked(localStorage.getItem('defaultAccount'), function(res) {
    if (!res) {
      alert("Time out Session, please re-Login");
      return;
    } else {
      if(localStorage.getItem('waitingCheck') !== 'false'){
          modal.style.display = "block";
      }
      dapp.deliveryBoy(carIdCode, function (res){
        console.log(res);
      });
    }
  });
});
