
var globalVars = require("../js/globalVars.js");
let $ = require('jquery');
var dapp = require("../js/dapp.js");
let fs = require('fs');

var modal = document.getElementById('myModal');

function addLoginOption(etherAddress, Customer){
  if(etherAddress){
    let updateOptions ='<option>'+etherAddress+', '+Customer+'</option>';
    $('#besitzerEther').append(updateOptions);
  }
}

$('#login').on('click', () => {

  let address = $('#besitzerEther').val().split(',');
  let password = $('#passwordEther').val();
  console.log('Ether '+address[0]);
  console.log('Password '+$('#passwordEther').val());
  if(localStorage.getItem('waitingCheck') !== 'false'){
      modal.style.display = "block";
  }
  dapp.loginBlockchain(address[0],password, function(result){
    if (result){
      alert("Login succesfull");
    } else {
      alert("Authentication Error!");
    }
      modal.style.display = "none";
      location.reload();
  });

});

$('#checkLogin').on('click', () => {
  let address = $('#besitzerEther').val().split(',');
  let password = $('#passwordEther').val();
  dapp.isAccountLocked(address[0], function(res){
    console.log(res);
    location.reload();
  });
});


function loadAndDisplayContacts() {  //Check if file exists
  if (fs.existsSync(globalVars.fileEtherAddress)) {
    let data = fs.readFileSync(globalVars.fileEtherAddress, 'utf8').split('\n')

    data.forEach((contact, index) => {
      let [etherAddress, Customer] = contact.split(',')
      console.log(etherAddress)
      addLoginOption(etherAddress, Customer)
    })

  } else {
    console.log("File Doesn\'t Exist. Creating new file.")
    fs.writeFile(filename, '', (err) => {
      if (err)
        console.log(err)
    })
  }
}

loadAndDisplayContacts();
