var globalVars = require("../js/globalVars.js");
var dapp = require("../js/dapp.js");
let $ = require('jquery')
let fs = require('fs')

var modal = document.getElementById('myModal');


// createNewCompound (string compoundName, uint freeSlots)
$('#create-new-compound').on('click', () => {
  let compoundName = $('#compoundName').val();
  let freeSlots = $('#freeSlots').val();
  console.log()
  if(compoundName.length > 0 && freeSlots > 0){
    if(localStorage.getItem('waitingCheck') !== 'false'){
        modal.style.display = "block";
    }
    dapp.createNewCompound(compoundName,freeSlots);
  } else {
    alert("Please set Values", "Error")
  }
})


dapp.getAllCompounds(function(res) {
  console.log(res);
  updateTable(res);
})

function updateTable(column){
  let newColumn ='<tr><td>'+
  column[0] + '</td><td>'+
  column[1] + '</td><td>' +
  column[2] + '</td></tr>';
  $('#compound-table').append(newColumn);
}
