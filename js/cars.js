var globalVars = require("../js/globalVars.js");
var dapp = require("../js/dapp.js");
let fs = require('fs')
let $ = require('jquery')
let row = 0;

var modal = document.getElementById('myModal');


$('#add-new-car').on('click', () => {
  if(localStorage.getItem('waitingCheck') !== 'false'){
    console.log("PLEASE WAITING SCREEN")
  } else {
    console.log("NO WAITING SCREEN")
  }


  let carDescription = $('#carDescription').val();
  let carIdCode = $('#carIdCode').val();
  //check if account is unlock
  dapp.isAccountLocked(localStorage.getItem('defaultAccount'), function(res) {
    if (!res) {
      alert("Time out Session, please re-Login");
      return;
    } else {
      if(localStorage.getItem('waitingCheck') !== 'false'){
          modal.style.display = "block";
      }
      dapp.addCar(carDescription, carIdCode);
    }
  });
});

$('#delete-car').on('click', () => {
  let carIdCode = $('#carIdCode-delete').val();
  //check if account is unlock
  dapp.isAccountLocked(localStorage.getItem('defaultAccount'), function(res) {
    if (!res) {
      alert("Time out Session, please re-Login");
      return;
    } else {
      if(localStorage.getItem('waitingCheck') !== 'false'){
          modal.style.display = "block";
      }
      dapp.deleteCar(carIdCode);
    }
  });
});

//change-carOwner  get driverAddress and carIdCode-newDriver
$('#change-carOwner').on('click', () => {
  let newOwner= $('#driverAddress').val().split(',');
  let carIdCode = $('#carIdCode-newDriver').val();
  //check if account is unlock
  dapp.isAccountLocked(localStorage.getItem('defaultAccount'), function(res) {
    if (!res) {
      alert("Time out Session, please re-Login");
      return;
    } else {
      if(localStorage.getItem('waitingCheck') !== 'false'){
          modal.style.display = "block";
      }
      dapp.changeCarOwner(newOwner[0],carIdCode);
    }
  });
});

$('#get-all-cars').on('click', () => {
  $("#cars-table tr>td").remove();
  dapp.getAllCars(function(res) {
    if(localStorage.getItem('waitingCheck') !== 'false'){
        modal.style.display = "block";
    }
    console.log(res);
    let updateString = '<tr><td>' +
      res[0] + '</td><td>' +
      res[1] + '</td><td>' +
      res[2] + '</td><td>' +
      '<select id=' + row + '>' +
      '<option>Allowed Driver </option>'+
      '</select>' +
      '</td><td>' +
      res[4] + '</td><td>' +
      res[5] + '</td></tr>';
    $('#cars-table').append(updateString);
    createDriverList(row, res[3]);
    row++;
    modal.style.display = "none";
  });
});

$('#get-car-by-idcode').on('click', () => {
  $("#cars-table tr>td").remove();
  dapp.getCarByCarIdCode($('#carIdCodeSearch').val(), function(res) {
    if(localStorage.getItem('waitingCheck') !== 'false'){
        modal.style.display = "block";
    }
      console.log(res[5]);
    let updateString = '<tr><td>' +
      res[2] + '</td><td>' + //owner
      res[3] + '</td><td>' + // desc
      res[4] + '</td><td>' + //idCode
      '<select id=' + row + '>' +
      '<option>Allowed Driver </option>'+
      '</select>' +
      '</td><td>' +
      res[6] + '</td><td>' +
      res[7] + '</td></tr>';
    $('#cars-table').append(updateString);
    createDriverList(row, res[5]);
    row++;
    modal.style.display = "none";
  });
});

$('#add-new-driver').on('click', () => {
  let newDriver = $('#driverAddress').val().split(',');
  let carIdCode = $('#carIdCode-newDriver').val();
  dapp.isAccountLocked(localStorage.getItem('defaultAccount'), function(res) {
    if (!res) {
      alert("Time out Session, please re-Login");
      return;
    } else {
      if(localStorage.getItem('waitingCheck') !== 'false'){
          modal.style.display = "block";
      }
      dapp.addNewDriver(newDriver[0], carIdCode);
    }
  });
});

$('#delete-driver').on('click', () => {
  let newDriver = $('#driverAddress').val().split(',');
  let carIdCode = $('#carIdCode-newDriver').val();
  dapp.isAccountLocked(localStorage.getItem('defaultAccount'), function(res) {
    if (!res) {
      alert("Time out Session, please re-Login");
      return;
    } else {
      if(localStorage.getItem('waitingCheck') !== 'false'){
          modal.style.display = "block";
      }
      dapp.deleteDriver(newDriver[0], carIdCode, function(res) {
        console.log(res);
      });
    }
  });
});

$('#unlock-car').on('click', () => {
  let carIDCode = $('#carIdCodeUnlock').val();
  dapp.isAccountLocked(localStorage.getItem('defaultAccount'), function(res) {
    if (!res) {
      alert("Time out Session, please re-Login");
      return;
    } else {
      if(localStorage.getItem('waitingCheck') !== 'false'){
          modal.style.display = "block";
      }
      dapp.unlockCar(carIDCode, function(res) {
        console.log(res);
      });
    }
  });
})

function loadAndDisplayContacts() { //Check if file exists
  if (fs.existsSync(globalVars.fileEtherAddress)) {
    let data = fs.readFileSync(globalVars.fileEtherAddress, 'utf8').split('\n')

    data.forEach((contact, index) => {
      let [etherAddress, Customer] = contact.split(',')
      addLoginOption(etherAddress, Customer)
    })

  } else {
    console.log("File Doesn\'t Exist. Creating new file.")
    fs.writeFile(filename, '', (err) => {
      if (err)
        console.log(err)
    })
  }
}

function addLoginOption(etherAddress, Customer) {
  if (etherAddress) {
    let updateOptions = '<option>' + etherAddress + ', ' + Customer + '</option>';
    $('#driverAddress').append(updateOptions);
  }
}

function createDriverList(optionsid, driverlist) {
  for (i = 0; i < driverlist.length; i++) {
    let updateOptions = '<option>' + driverlist[i] + '</option>';
    $('#'+optionsid).append(updateOptions);
  }

}

loadAndDisplayContacts();
