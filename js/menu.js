var dapp = require('../js/dapp.js')
var globalVar = require('../js/globalVars.js')

function loadMenu() {
  let $ = require('jquery')
  let curPage = document.title;
  let menuentr = "";
  var pages = ["QuickGate", "Login", "Cars", "Compounds", "Orders", "DeliveryBoy"];
  menuentry = "<li><a href=\"\" ><img src=\"../static/refresh.png\" height=\"30\"></a></li>"
  $('ul').append(menuentry);
  menuentry = "<li>Chain<br>Sync.<br><input id=\"checkBox\" type=\"checkbox\"></li>"
  $('ul').append(menuentry);
  $("#checkBox").prop("checked", localStorage.getItem('waitingCheck') !== 'false');
  console.log(localStorage.getItem('waitingCheck') === 'true');
  for (i = 0; i < pages.length; i++) {
    if (curPage === pages[i]) {
      //set active
      menuentry = "<li class=\"active\"><a href=\"#\">" + curPage + "</a></li>"
    } else {
      // not active
      menuentry = "<li><a href=\"../views/" + pages[i] + ".html\">" + pages[i] + "</a></li>"
    }
    $('ul').append(menuentry);
  }
  menuentry = "<li style=\"background-color:red;\">Not logged in</li>"
  $('ul').append(menuentry);
  dapp.isAccountLocked(localStorage.getItem('defaultAccount'), function(res) {
    var select = document.getElementById('menubar');
    select.removeChild(select.lastChild);
    if (res) {
      menuentry = "<li style=\"background-color:green;\">Logged in as: <br>" + localStorage.getItem('defaultAccount') + "</li>"
    } else {
      menuentry = "<li style=\"background-color:red;\">Time out: ReLogin: <br>" + localStorage.getItem('defaultAccount') + "</li>"
    }
    $('ul').append(menuentry);
  })
  console.log(curPage);

  $('#checkBox').change(function(){
    if (localStorage.getItem('waitingCheck') === null){
      localStorage.setItem('waitingCheck', true);
    } else {
      let bool = localStorage.getItem('waitingCheck') !== 'false';
      localStorage.setItem('waitingCheck', !bool);
    }
  })

}

loadMenu();
