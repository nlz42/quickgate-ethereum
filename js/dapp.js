var Web3 = require('web3');
var net = require('net');
var httpprovider = new Web3.providers.HttpProvider('http://localhost:8545');
var wsprovider = new Web3.providers.WebsocketProvider('ws://localhost:8546');
var web3WS = new Web3(wsprovider);
var web3 = new Web3(httpprovider);
var Eth = require('web3-eth');
var globalVars = require("../js/globalVars.js");
var carsOnSmartContract;
var balanceMain = 0;
var knownAccounts = [];
var gasLimit = 470000;
//events vars:
var driveradded;
var driverdeleted;
var caradded;
var carunlocked;
var compoundadded;
var orderadded;
var deliverysuccess;
var cardeleted;
var carOwnerchanged;
//fehletut: , {orderdeleted->its private}

module.exports = {

  isAccountLocked: function(account, callback) {
    let isLocked = true
    localStorage.setItem('defaultAccount', account);

    web3.eth.sendTransaction({
      from: account,
      to: account,
      value: 0
    }, function(result) {
      if (result) {
        console.log("Authentication Error!", 'Error');
        result1 = false;
      } else {
        console.log("Logged in", 'Success');
        result1 = true;
      }
      if (callback) {
        callback(result1);
      }
    });
  },

  loginBlockchain: function(address, password, callback) {
    localStorage.setItem('defaultAccount', address);
    web3.eth.personal.unlockAccount(address, password, 3600, function(result) {
      if (result) {
        callback(false);
      } else {
        callback(true);
      }
    });
  },

  addCar: function addCar(carOwnerName, carIdCode) {
    connectToSmartContract();
    if (typeof caradded !== "undefined") {
      console.log("event already subscribed")
    } else {
      caradded = carsOnSmartContract.events.caradded({}, '', function(error, result) {
        if (result.returnValues[1]) {
          alert("New car added","Success");
        } else {
          alert("Error! Something goes wrong, please check connection.", "Error")
        }
        modal.style.display = "none";
      });
    }

    carsOnSmartContract.methods.createNewCar(carOwnerName, carIdCode).send({
      from: localStorage.getItem('defaultAccount'),
      gas: gasLimit
    }, function(error, contract) {
      if (error) {
        console.log(error);
      }
    });
  },

  deleteCar: function deleteCar (carIdCode) {
    connectToSmartContract();
    if (typeof cardeleted !== "undefined"){
      console.log("event already subscribed")
    } else {
      cardeleted = carsOnSmartContract.events.cardeleted({}, '', function(error, result) {
        if (result.returnValues[1]) {
          alert("Car deleted","Success");
        } else {
          alert("Error! You are not allwowed for this action!","Error")
        }
        modal.style.display = "none";
      });
    }
    carsOnSmartContract.methods.deleteCarByCarIdCode(carIdCode).send({
      from: localStorage.getItem('defaultAccount'),
      gas: gasLimit
    }, function (error, contract){
      if (error) {
        console.log(error);
      }
    });
  },

  changeCarOwner: function changeCarOwner(newOwner,carIdCode){
    connectToSmartContract();
    if (typeof carOwnerchanged !== "undefined"){
      console.log("event already subscribed")
    } else {
      carOwnerchanged = carsOnSmartContract.events.carOwnerchanged({}, '', function(error, result) {
        if (result.returnValues[1]) {
          alert("Owner Changed","Success");
        } else {
          alert("Error! You not the owner of the car!","Error")
        }
        modal.style.display = "none";
      });
    }
    carsOnSmartContract.methods.changeCarOwner(newOwner,carIdCode).send({
      from: localStorage.getItem('defaultAccount'),
      gas: gasLimit
    }, function (error, contract){
      if (error) {
        console.log(error);
      }
    });
  },

  getAllCars: function getAllCars(callback) {
    connectToSmartContract();
    carsOnSmartContract.methods.getCountCars().call(function(err, res) {
      console.log(res);
      if (err) {
        console.log(error);
      }
      for (i = 0; i < res; i++) {
        carsOnSmartContract.methods.getCarByArrayPos(i).call(function(err, res) {
          if (err) {
            console.log(err);
          }
          callback(res);
        });
      }
    });
  },

  getCarByCarIdCode: function getCarByCarIdCode(carIdCode, callback) {
    connectToSmartContract();
    var result = "";
    carsOnSmartContract.methods.getCarByCarIdCode(carIdCode).call(function(err, res) {
      result = res;
      if (err) {
        console.log(err);
      }
      if (callback) {
        callback(res);
      }
    });
  },

  addNewDriver: function addNewDriver(newDriver, carIdCode) {
    connectToSmartContract();
    if (typeof driveradded !== "undefined") {
      console.log("event already subscribed")
    } else {
      driveradded = carsOnSmartContract.events.driveradded({}, '', function(error, result) {
        if (result.returnValues[1]) {
          alert("Driver succesful Added.","Success");
        } else {
          alert("False Driver Address, IdCode or you're not the Owner!","Error")
        }
        modal.style.display = "none";
      });
    }

    carsOnSmartContract.methods.addNewDriver(newDriver, carIdCode).send({
      from: localStorage.getItem('defaultAccount'),
      gas: gasLimit
    }, function(error, contract) {
      if (error) {
        console.log(error);
      }
    });
  },

  deleteDriver: function deleteDriver(driverAddress, carIdCode, callback) {
    connectToSmartContract();
    if (typeof driverdeleted !== "undefined") {
      console.log("event already subscribed")
    } else {
      driverdeleted = carsOnSmartContract.events.driverdeleted({}, '', function(error, result) {
        if (result.returnValues[1]) {
          alert("Driver succesful deleted.","Success");
        } else {
          alert("False Driver Address, IdCode or you're not the Owner!","Error")
        }
        modal.style.display = "none";
      });
    }
    carsOnSmartContract.methods.deleteDriver(driverAddress, carIdCode).send({
      //address driver, string carIdCode
      from: localStorage.getItem('defaultAccount'),
      gas: gasLimit
    }, function(error, contract) {
      if (error) {
        console.log(error);
      }
    });
  },

  unlockCar: function unlockCar(carID, callback) {
    //unlockCar(string carIdCode)
    connectToSmartContract();
    if (typeof carunlocked !== "undefined") {
      console.log("event already subscribed")
    } else {
      carunlocked = carsOnSmartContract.events.carunlocked({}, '', function(error, result) {
        if (result.returnValues[1]) {
          alert("Car unlocked. Please check new costs!","Success");
        } else {
          alert("Error. Car not locked or you are not the compound owner!","Error")
        }
        modal.style.display = "none";
      });
    }
    carsOnSmartContract.methods.unlockCar(carID).send({
      from: localStorage.getItem('defaultAccount'),
      gas: gasLimit
    }, function(error, contract) {
      if (error) {
        console.log(error);
      }
    });
  },

  getAllCompounds: function getAllCompounds(callback) {
    connectToSmartContract();
    carsOnSmartContract.methods.getCountCompounds().call(function(err, res) {
      if (err) {
        console.log(err);
      }
      console.log(res);
      for (i = 0; i < res; i++) {
        carsOnSmartContract.methods.getCompoundByArryaPos(i).call(function(err, res) {
          if (err) {
            console.log(err);
          }
          console.log(res);
          callback(res);
        })
      }
    })
  },

  createNewCompound: function createNewCompound(compoundName, freeSlots, callback) {
    connectToSmartContract();
    if (typeof compoundadded !== "undefined") {
      console.log("event already subscribed")
    } else {
      compoundadded = carsOnSmartContract.events.compoundadded({}, '', function(error, result) {
        if (result.returnValues[1]) {
          alert("Compound added.","Success");
        } else {
          alert("Error, please check Login state.","Error")
        }
        modal.style.display = "none";
      })
    }

    carsOnSmartContract.methods.createNewCompound(compoundName, freeSlots).send({
      from: localStorage.getItem('defaultAccount'),
      gas: gasLimit
    }, function(error, contract) {
      if (error) {
        console.log(error);
      }
    })
  },

  createNewOrder: function createNewOrder(carIdCode, compoundName, time, delivery, callback) {
    connectToSmartContract();
    if (typeof orderadded !== "undefined") {
      console.log("event already subscribed")
    } else {
      orderadded = carsOnSmartContract.events.orderadded({}, '', function(error, result) {
        if (result.returnValues[1]) {
          alert("New order added","Success")
        } else {
          alert("Error, please check dataset","Error");
        }
        modal.style.display = "none";
      });
    }
    console.log("dapp deliverystate: " + delivery);
    carsOnSmartContract.methods.createOrder(carIdCode, compoundName, time, delivery).send({
      from: localStorage.getItem('defaultAccount'),
      gas: gasLimit
    }, function(error, contract) {
      if (error) {
        console.log(error);
      }
    });
  },

  getAllOrders: function getAllOrders(callback) {
    connectToSmartContract();
    carsOnSmartContract.methods.getCountOrders().call(function(err, res) {
      if (err) {
        console.log(err);
      }
      console.log(res);
      for (i = 0; i < res; i++) {
        carsOnSmartContract.methods.getOrderByArrayPos(i).call(function(err, res) {
          if (err) {
            console.log(err);
          }
          console.log(res);
          callback(res);
        })
      }
    })
  },

  deliveryBoy: function deliveryBoy(carIdCode, callback) {
    connectToSmartContract();
    if (typeof deliverysuccess !== "undefined") {
      console.log("event already subscribed")
    } else {
    deliverysuccess = carsOnSmartContract.events.deliverysuccess({}, '', function(error, result) {
      console.log(result);
      alert(result.returnValues[1], "Change State")
      modal.style.display = "none";
    });
  }
    carsOnSmartContract.methods.deliveryBoy(carIdCode).send({
      from: localStorage.getItem('defaultAccount'),
      gas: gasLimit
    }, function(error, contract) {
      if (error) {
        console.log(error);
      }
    });
  }
}

function connectToSmartContract() {
  if (typeof carsOnSmartContract !== "undefined") {
    console.log('connection already exist')
  } else {
    carsOnSmartContract = new web3WS.eth.Contract(globalVars.contractABI, globalVars.contractAddress);
  }

}
