var globalVars = require("../js/globalVars.js");
let filename = globalVars.fileEtherAddress;
let sno = 0;

$('#add-to-list').on('click', () => {
  let etherAddress = $('#etherAddress').val()
  let Customer = $('#Customer').val()

  fs.appendFile(filename, etherAddress + ',' + Customer + '\n')

  addEntry(etherAddress, Customer)
})

function addEntry(etherAddress, Customer) {
  if (etherAddress && Customer) {
    sno++
    let updateString = '<tr><td>' + sno + '</td><td>' + etherAddress + '</td><td>' +
      Customer + '</td></tr>'
    $('#contact-table').append(updateString)
  }
}

function loadAndDisplayContacts() {

  //Check if file exists
  if (fs.existsSync(filename)) {
    let data = fs.readFileSync(filename, 'utf8').split('\n')

    data.forEach((contact, index) => {
      let [etherAddress, Customer] = contact.split(',')
      addEntry(etherAddress, Customer)
    })

  } else {
    console.log("File Doesn\'t Exist. Creating new file.")
    fs.writeFile(filename, '', (err) => {
      if (err)
        console.log(err)
    })
  }
}

function addRowHandlers() {
  var table = document.getElementById("contact-table");
  var rows = table.getElementsByTagName("tr");
  for (i = 0; i < rows.length; i++) {
    var currentRow = table.rows[i];
    var createClickHandler =
      function(row) {
        return function() {
          let cell = row.getElementsByTagName("td");
          let id = cell[0].innerHTML;
          let etherAddress = cell[1].innerHTML;
          alert("id: " + id + "\n" +
            "Address: " + etherAddress + "\n" +
            "Details: " + cell[2].innerHTML, etherAddress);
        };
      };
    currentRow.onclick = createClickHandler(currentRow);
  }
}

loadAndDisplayContacts();
addRowHandlers();
