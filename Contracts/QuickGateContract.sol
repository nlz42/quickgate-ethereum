pragma solidity ^0.4.18;
contract V1_0_QuickGateContract{

  address public contractOwner;
  Car[] private cars;
  Compound[] private compounds;
  Order[] private orders;

  function V1_0_QuickGateContract() public {
    contractOwner = msg.sender;
  }

  function close()  public {
      if(msg.sender == contractOwner){
          selfdestruct(contractOwner);
      }
  }

  struct Car {
    address carOwner;
    string carDescription;
    string carIdCode;
    address[] allowedDriver;
    Compound compound;
    bool isLocked;
  }

  struct Compound {
    address compoundOwner;
    string compoundName;
    uint freeSlots;
  }

  struct Order {
    address orderCreator;
    string carIdCode;
    uint planedExecutionTime; // plus minus 1 hour is free!
    uint payToCompound;
    uint payToDriver;
    bool deliverCar; // true: deliver to compound, false: pick up from compound
  }

  modifier updateCarLockState () {
    for (uint i=0; i<orders.length; i++){
      uint diff = 0;
      uint carPos;
      if(orders[i].planedExecutionTime < now){
        diff = now - orders[i].planedExecutionTime;
      } else {
        diff = orders[i].planedExecutionTime - now;
      }
      (carPos,) = getCarByCarIdCode(orders[i].carIdCode);
      if (diff > 3600){
        cars[carPos].isLocked = true;
      } else {
        cars[carPos].isLocked = false;
      }
    }
    _;
  }

  modifier checkCarArray(uint pos){
      if (pos < cars.length){
          _;
      } else {
         revert();
      }
  }

  modifier checkCompoundArray(uint pos){
      if (pos < compounds.length){
          _;
      } else {
         revert();
      }
  }

  modifier checkOrdersArray (uint pos){
    if(pos < orders.length){
      _;
    } else {
      revert();
    }
  }

  event caradded(address, bool);
  event cardeleted(address, bool);
  event driveradded(address,bool);
  event compoundadded(address,bool);
  event orderadded(address,bool);
  event orderdeleted(address,bool);
  event deliverysuccess(address, string);
  event carunlocked(address, bool);
  event driverdeleted (address, bool);
  event carOwnerchanged(address, bool);

  function createNewCar(string carDescription, string carIdCode) updateCarLockState() public {
    for (uint i = 0; i < cars.length ; i++){
      if (keccak256(cars[i].carIdCode) == keccak256(carIdCode)){
        caradded(msg.sender, false);
        return;
      }
    }
      uint size = cars.length++;
      cars[size].carOwner = msg.sender;
      cars[size].carDescription = carDescription;
      cars[size].carIdCode = carIdCode;
      caradded(msg.sender, true);
  }

  function deleteCarByCarIdCode (string carIdCode) updateCarLockState() public returns (bool) {
    uint pos = 0;
    bool found;
    bool foundOrder;
    (pos, found,) = getCarByCarIdCode(carIdCode);
    (,foundOrder) = getOrderByCarIdCode(carIdCode);
    if (cars[pos].carOwner == msg.sender && found && !(cars[pos].isLocked) && !(foundOrder)){
      cars[pos] = cars[cars.length-1];
      delete cars[cars.length-1];
      cars.length--;
      cardeleted(msg.sender, true);
      return true;
    } else {
      cardeleted(msg.sender, false);
      return false;
    }
  }

  function getCarByCarIdCode (string carIdCode) public view returns (uint, bool, address, string, string,address[],string,bool){
    for (uint pos=0;pos<cars.length;pos++){
      if( keccak256(cars[pos].carIdCode) == keccak256(carIdCode) ){
        return (
          pos,
          true,
          cars[pos].carOwner,
          cars[pos].carDescription,
          cars[pos].carIdCode,
          cars[pos].allowedDriver,
          cars[pos].compound.compoundName,
          cars[pos].isLocked);
      }
    }
  }

  function getCarByArrayPos (uint pos) checkCarArray(pos) public view returns (address, string, string,address[],string,bool){
    return (
      cars[pos].carOwner,
      cars[pos].carDescription,
      cars[pos].carIdCode,
      cars[pos].allowedDriver,
      cars[pos].compound.compoundName,
      cars[pos].isLocked);
  }

  function addNewDriver (address newDriver, string carIdCode) updateCarLockState() public returns (bool) {
    uint carPos;
    bool found = false;
    (carPos,found,) = getCarByCarIdCode(carIdCode);
    if(cars[carPos].carOwner == msg.sender && found){
      cars[carPos].allowedDriver.push(newDriver);
      driveradded(msg.sender,true);
      return true;
    } else {
      driveradded(msg.sender,false);
      return false;
    }
  }

  function changeCarOwner (address newOwner, string carIdCode) public returns (bool){
    uint carPos;
    bool found;
    (carPos,found,) = getCarByCarIdCode(carIdCode);
    if (found && cars[carPos].carOwner == msg.sender){
      cars[carPos].carOwner = newOwner;
      carOwnerchanged(msg.sender, true);
      return true;
    } else {
      carOwnerchanged(msg.sender, false);
      return false;
    }
  }

  function deleteDriver (address driver, string carIdCode) updateCarLockState() public returns (bool) {
    uint carPos;
    bool found = false;
    (carPos,found,) = getCarByCarIdCode(carIdCode);
    if(cars[carPos].carOwner == msg.sender && found){
      for (uint pos=0;pos<cars[carPos].allowedDriver.length;pos++){
        if(cars[carPos].allowedDriver[pos] == driver){
          cars[carPos].allowedDriver[pos] = cars[carPos].allowedDriver[cars[carPos].allowedDriver.length-1];
          delete cars[carPos].allowedDriver[cars[carPos].allowedDriver.length-1];
          cars[carPos].allowedDriver.length--;
          driverdeleted (msg.sender, true);
          return true;
        }
      }
      driverdeleted (msg.sender, false);
      return false;
    } else {
      driverdeleted (msg.sender, false);
      return false;
    }
  }


  function createNewCompound (string compoundName, uint freeSlots) updateCarLockState() public {
    for (uint i = 0; i < compounds.length ; i++){
      if (keccak256(compounds[i].compoundName) == keccak256(compoundName)){
        compoundadded(msg.sender,false);
        revert();
      }
    }
      Compound memory cmp = Compound(msg.sender,compoundName,freeSlots);
      compounds.push(cmp);
      compoundadded(msg.sender,true);
  }

  function getCompoundByCompoundName (string cmpName) public view returns (uint, bool, address, string, uint) {
    for (uint pos=0;pos<cars.length;pos++){
      if( keccak256(compounds[pos].compoundName) == keccak256(cmpName) ){
        return (
          pos,
          true,
          compounds[pos].compoundOwner,
          compounds[pos].compoundName,
          compounds[pos].freeSlots
          );
        }
      }
  }

  function getCompoundByArryaPos(uint pos) checkCompoundArray(pos) public view returns (address,string,uint){
      return(
          compounds[pos].compoundOwner,
          compounds[pos].compoundName,
          compounds[pos].freeSlots
          );
  }

  function createOrder(string carIdCode, string compoundName, uint time, bool deliverCar) updateCarLockState() public returns (bool){
    uint carPos;
    uint cmpPos;
    bool foundCar;
    bool foundCmp;
    bool foundOrder;
    (carPos,foundCar,) = getCarByCarIdCode(carIdCode);
    (cmpPos, foundCmp,) = getCompoundByCompoundName(compoundName);
    (,foundOrder) = getOrderByCarIdCode(carIdCode);
    if(foundOrder){
      orderadded(msg.sender,false);
      return false;
    }
    if (foundCar && foundCmp){
      Order memory order = Order(msg.sender,carIdCode,time,5,5, deliverCar);
      orders.push(order);
      if(deliverCar && compounds[cmpPos].freeSlots > 0){
        compounds[cmpPos].freeSlots -= 1;
        cars[carPos].compound = compounds[cmpPos];
        orderadded(msg.sender,true);
        return (true);
      }
      if (!deliverCar){
        compounds[cmpPos].freeSlots += 1;
        cars[carPos].compound = compounds[cmpPos];
        orderadded(msg.sender,true);
        return (true);
      }
      orderadded(msg.sender,false);
      return (false);
    } else {
      orderadded(msg.sender,false);
      return (false);
    }
  }

  function getOrderByCarIdCode(string carIdCode) public view returns (uint, bool) {
    for (uint pos=0;pos<orders.length;pos++){
      if( keccak256(orders[pos].carIdCode) == keccak256(carIdCode) ){
        return (pos, true);
        }
      }
      return (0,false);
  }

  function getOrderByArrayPos(uint orderPos) checkOrdersArray(orderPos) public view returns (string, uint,uint,uint, bool){
    return (
    orders[orderPos].carIdCode,
    orders[orderPos].planedExecutionTime, // plus minus 1 hour is free!
    orders[orderPos].payToCompound,
    orders[orderPos].payToDriver,
    orders[orderPos].deliverCar); // true: deliver to compound, false: pick up from compound
  }

  function deleteOrderByOrderPos (uint orderPos) private {
      orders[orderPos] = orders[orders.length-1];
      delete orders[orders.length-1];
      orders.length--;
      orderdeleted(msg.sender,true);
  }

  function deliveryBoy (string carIdCode) updateCarLockState() public returns (string) {
    uint carPos;
    bool foundCar;
    uint orderPos;
    bool foundOrder;
    (carPos,foundCar,) = getCarByCarIdCode(carIdCode);
    (orderPos,foundOrder) = getOrderByCarIdCode(carIdCode);
    if(!(foundCar) || !(foundOrder)){
        deliverysuccess(msg.sender, "Order or Car not found!");
        return ("Order or Car not found!");
    }
      bool driverAllowed = false;
      for (uint i=0; i < cars[carPos].allowedDriver.length;i++){
          if(msg.sender == cars[carPos].allowedDriver[i]){
              driverAllowed = true;
          }
      }
      if(!driverAllowed){
         deliverysuccess(msg.sender, "Driver not Allowed");
         return ("Driver not Allowed");
      }
      if (cars[carPos].isLocked){
         deliverysuccess(msg.sender, "Please Unlock Car");
         return ("Please Unlock Car");
      }
      deleteOrderByOrderPos(orderPos);
      delete cars[carPos].compound;
      deliverysuccess(msg.sender, "Order success");
      return("Order success");
 }

  function unlockCar(string carIdCode) updateCarLockState() public returns (bool){
  uint carPos;
  bool foundCar;
  uint orderPos;
  bool foundOrder;
  (carPos,foundCar,) = getCarByCarIdCode(carIdCode);
  (orderPos,foundOrder) = getOrderByCarIdCode(carIdCode);
  if(foundCar && msg.sender == cars[carPos].compound.compoundOwner && foundOrder){
    cars[carPos].isLocked = false;
    orders[orderPos].planedExecutionTime = now;
    orders[orderPos].payToCompound = orders[orderPos].payToCompound+10;
    carunlocked(msg.sender, true);
    return true;
  } else {
    carunlocked(msg.sender, false);
    return false;
  }
 }

  function getCountOrders() public view returns(uint) {
    return orders.length;
  }
  function getCountCars() public view returns (uint){
    return cars.length;
  }
  function getCountCompounds() public view returns (uint){
    return compounds.length;
  }
}
